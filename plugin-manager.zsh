#!/bin/zsh

function plugin-load {
    local url plugdir initfile initfiles=()
    #: ${ZPLUGINDIR:=${ZDOTDIR:-~/.config/zsh}/plugins}
    for url in $@; do
	plugdir=$ZDOTDIR/plugins/${url:t}
	initfile=$plugdir/${url:t}.plugin.zsh
	if [[ ! -d $plugdir ]]; then
	    echo "Cloning $url..."
	    git clone -q --depth 1 --recursive --shallow-submodules $url \
	    	$plugdir
	fi
	if [[ ! -e $initfile ]]; then
	    initfiles=($plugdir/*.{plugin.zsh,zsh-theme,zsh,sh}(N))
	    (( $#initfiles )) || { echo >&2 "No init file '$url'." && continue }
	    ln -sf $initfiles[1] $initfile
	fi
	fpath+=$plugdir
	(( $+functions[zsh-defer] )) && zsh-defer . $initfile || . $initfile
    done
}
