# XDG
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# Editor
export EDITOR="nvim"

# zsh
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export HISTFILE="$ZDOTDIR/.zhistory"
export HISTSIZE=10000
export SAVEHIST=10000

# X11
# Note that these variables are respected by xinit, but not by startx
#export XINITRC="$XDG_CONFIG_HOME/X11/xinitrc"
#export XSERVERRC="$XDG_CONFIG_HOME/X11/xserverrc"

# Doom
#export DOOMWADDIR="$HOME/games/doom/wads"

# SDL
#export SDL_FORCE_SOUNDFONT=1
#export SDL_SOUNDFONT="/usr/share/sounds/sf2"

# Pass
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"

# Add ~/.local/bin to the PATH
typeset -U path PATH
path=($HOME/.local/bin $path)
export PATH
