# Start MPD daemon as user to not use global system configuration
#mpd

# Launch X by using the xinitrc file in ~/.config
# Try only if X is not already running
#if ! xhost >& /dev/null; then
	#startx "$XDG_CONFIG_HOME/X11/xinitrc"
#fi
