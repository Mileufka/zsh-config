# Created by newuser for 5.9

if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
    source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

if [ -d $XDG_CONFIG_HOME/shell-common/ ]; then

    # Aliases
    if [ -f $XDG_CONFIG_HOME/shell-common/aliases ]; then
	source $XDG_CONFIG_HOME/shell-common/aliases
    fi

    # Paths
    if [ -f $XDG_CONFIG_HOME/shell-common/paths ]; then
	source $XDG_CONFIG_HOME/shell-common/paths
    fi

    # WSL
    if [[ -f $XDG_CONFIG_HOME/shell-common/wslrc && -n "$WSLENV" ]]; then
	source $XDG_CONFIG_HOME/shell-common/wslrc
    fi
fi

# History
setopt SHARE_HISTORY		# Share history between all sessions
setopt HIST_EXPIRE_DUPS_FIRST	# Expire a duplicate event first when trimming history
setopt HIST_IGNORE_DUPS		# Do not record an event that was just recorded again
setopt HIST_SAVE_NO_DUPS	# Do not write a diplicate event to the history file

# Completion
autoload -U compinit
compinit
#promptinit

source <(fzf --zsh)
setopt APPENDHISTORY

if [ -f /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme ]; then
    source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
elif [ -f $ZDOTDIR/themes/powerlevel10k/powerlevel10k.zsh-theme ]; then
    source $ZDOTDIR/themes/powerlevel10k/powerlevel10k.zsh-theme
fi

if zmodload zsh/terminfo && (( terminfo[colors] >= 256 )); then
    # To customize prompt, run `p10k configure` or edit ~/.config/zsh/p10k.zsh
    [[ ! -f $ZDOTDIR/p10k.zsh ]] || source $ZDOTDIR/p10k.zsh
else
    [[ ! -f $ZDOTDIR/p10k-portable.zsh ]] || source $ZDOTDIR/p10k-portable.zsh
fi

if [ ! -d $ZDOTDIR/plugins ]; then
    mkdir -pv $ZDOTDIR/plugins
fi

source $ZDOTDIR/plugin-manager.zsh
repos=(
    https://github.com/zsh-users/zsh-autosuggestions
    https://github.com/zsh-users/zsh-syntax-highlighting
    https://github.com/rupa/z
)

plugin-load $repos

ZSH_THEME="powerlevel10k/powerlevel10k"
